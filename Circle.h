/**
 * @file    Circle.h
 * @authors <FILL ME IN ACCORDINGLY>
 * @brief   Circle specification.
 */

#ifndef CIRCLE_H
#define CIRCLE_H

#include "Shape.h"

double const PI = 3.14159265358979323846;

class Circle : public Shape {
public:
    /**
     * Default Circle constructor. 
     * 
     * @post A default circle is taken to be the unit circle and as such, the
     * radius of a default Circle is equal to 1.
     */
    Circle();
    
    /**
     * Parameterized Circle constructor.
     * 
     * @post A new Circle is created with the given radius. If the supplied 
     * radius is negative, i.e., less than zero, then the radius is adjusted
     * automatically to the default radius of 1.
     * @param radius the radius of this new Circle
     */
    Circle(const double& radius);
    
    /**
     * Obtain the radius of this Circle.
     * 
     * @return The radius of this circle is returned.
     */
    double getRadius() const;
    
    /**
     * Modify the radius of this Circle.
     * 
     * @post The radius of this circle has been modified. If the given radius is
     * negative, i.e., less than zero, then the radius is adjusted automatically
     * to the default radius of 1, otherwise it has been changed to the given
     * value.
     * @param radius the new radius for this Circle
     */
    void setRadius(const double& radius);
    
    /**
     * @copydoc Shape::getArea() 
     */
    virtual double getArea() const override;
    
    /**
     * @copydoc Shape::getPerimeter() 
     */
    virtual double getPerimeter() const override;
    
    /**
     * Circle destructor.
     */
    virtual ~Circle();
private:
    double radius;
};

#endif /* CIRCLE_H */

