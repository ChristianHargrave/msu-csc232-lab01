/**
 * @file    Rectangle.h
 * @authors <FILL ME IN ACCORDINGLY>
 * @brief   Rectangle specification.
 */

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.h"

class Rectangle : public Shape {
public:
    /**
     * Default Rectangle constructor. 
     * 
     * @post A default rectangle is specified to have a length of 2 and a width
     * of 1.
     */
    Rectangle();
    
    /**
     * Parameterized Rectangle constructor.
     * 
     * @post A new Rectangle is created with the given length and width. When 
     * negative, i.e., less than zero, parameters are given, they are
     * individually set to their corresponding defaults of 2 and 1 respectively
     * for length and width.
     * 
     * @param length the length of this Rectangle
     * @param width the width of this Rectangle
     */
    Rectangle(const double& length, const double& width);
    
    /**
     * Obtain the length of this Rectangle.
     * 
     * @return The length of this Rectangle is returned.
     */
    double getLength() const;
    
    /**
     * Modify the length of this Rectangle.
     * 
     * @post The length of this Rectangle has been modified. If the given length
     * is negative, i.e., less than 0, then the length is automatically adjusted
     * to the default length of 2, otherwise it has been changed to the given 
     * value.
     * 
     * @param length the new length for this Rectangle
     */
    void setLength(const double& length);
    
    /**
     * Obtain the width of this Rectangle.
     * 
     * @return The width of this Rectangle is returned.
     */
    double getWidth() const;
    
    /**
     * Modify the width of this Rectangle.
     * 
     * @post The width of this Rectangle has been modified. If the given width
     * is negative, i.e., less than 0, then the width is automatically adjusted
     * to the default width of 1, otherwise it has been changed to the given
     * value.
     * @param width the new width of this Rectangle
     */
    void setWidth(const double& width);
    
    /**
     * @copydoc Shape::getArea() 
     */
    virtual double getArea() const override;
    
    /**
     * @copydoc Shape::getPerimeter() 
     */
    virtual double getPerimeter() const override;
    virtual ~Rectangle();
private:
    double length;
    double width;
};

#endif /* RECTANGLE_H */

