/**
 * @file   RectangleTest.h
 * @author Jim Daehn
 * @brief  Specification of Rectangle Unit Test. DO NOT MODIFY THE CONTENTS 
 * OF THIS FILE! ANY MODIFICATION TO THIS FILE WILL RESULT IN A GRADE OF 0 FOR
 * THIS LAB!
 */

#ifndef RECTANGLETEST_H
#define RECTANGLETEST_H

#include <cppunit/extensions/HelperMacros.h>

class RectangleTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(RectangleTest);

    CPPUNIT_TEST(testDefaultRectangleLength);
    CPPUNIT_TEST(testDefaultRectangleWidth);
    CPPUNIT_TEST(testDefaultRectangleArea);
    CPPUNIT_TEST(testDefaultRectanglePerimeter);
    CPPUNIT_TEST(testMutatorSetsDefaultLengthGivenNegativeInput);
    CPPUNIT_TEST(testMutatorSetsDefaultWidthGivenNegativeInput);
    CPPUNIT_TEST(testParameterizedRectangleArea);
    CPPUNIT_TEST(testParameterizedRectanglePerimeter);

    CPPUNIT_TEST_SUITE_END();

public:
    RectangleTest();
    virtual ~RectangleTest();
    void setUp();
    void tearDown();

private:
    void testDefaultRectangleLength();
    void testDefaultRectangleWidth();
    void testDefaultRectangleArea();
    void testDefaultRectanglePerimeter();
    void testMutatorSetsDefaultLengthGivenNegativeInput();
    void testMutatorSetsDefaultWidthGivenNegativeInput();
    void testParameterizedRectangleArea();
    void testParameterizedRectanglePerimeter();
};

#endif /* RECTANGLETEST_H */

