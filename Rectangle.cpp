/**
 * @file    Rectangle.cpp
 * @authors <FILL ME IN ACCORDINGLY>
 * @brief   Rectangle class implementation.
 */

#include "Rectangle.h"

Rectangle::Rectangle() {
    // TODO:  Remove this comment and ensure that the length and width of this
    // Rectangle are set as per specification.
}

Rectangle::Rectangle(const double& rectLength, const double& rectWidth) {
    // TODO:  Remove this comment and ensure that the length and width of this
    // Rectangle are set as per specification.
}

double Rectangle::getLength() const {
    // TODO: Remove this comment and return the correct value.
    return 0;
}

void Rectangle::setLength(const double& rectLength) {
    // TODO: Remove this comment and ensure that the length is set as per
    // specification.
}

double Rectangle::getWidth() const {
    // TODO: Remove this comment and return the correct value.
    return 0;
}

void Rectangle::setWidth(const double& rectWidth) {
    // TODO: Remove this comment and ensure that the width is set as per
    // specification.
}

double Rectangle::getArea() const {
    // TODO: Remove this comment and ensure the correct area is computed and 
    // returned accordingly.
    return 0;
}

double Rectangle::getPerimeter() const {
    // TODO: Remove this comment and ensure the correct perimeter is computed  
    // and returned accordingly.
    return 0;
}

Rectangle::~Rectangle() {
}

